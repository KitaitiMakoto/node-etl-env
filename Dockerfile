FROM node:16

WORKDIR /app
RUN apt update -y && \
        apt install -y libnss3 libatk-bridge2.0 libx11-xcb1 libxcb-dri3-0 libdrm2 libgbm1 libasound2 libgtk-3-0 && \
        apt clean && rm -rf /var/lib/apt/lists/* && \
        npm install "@google-cloud/bigquery@5.3.0" "csv@5.3.2" "iconv-lite@0.6.2" "puppeteer@5.3.1" "tmp-promise@3.0.2" "luxon@1.25.0" "yargs@17.0.1" && \
        npm install -g sfdx-cli npm && \
        wget https://cache.ruby-lang.org/pub/ruby/3.1/ruby-3.1.1.tar.gz && \
        tar xsfv ruby-3.1.1.tar.gz && \
        cd ruby-3.1.1 && \
        ./configure && \
        make && \
        make install && \
        cd - && \
        rm -rf ruby-3.1.1 && \
        gem install --no-document futureshop -v 0.1.6
